# Algorytmy genetyczne - zadanie komiwojażera
- Weronika Żmuda-Trzebiatowska 172179
- Michał Kuczkowski 171546

## Wprowadzenie do zadania

Nazwa zadania nawiązuje do komiwojażera, który ma odwiedzić określoną ilość miast i na koniec wrócić do miejsca, z którego wyruszał. Drogę należy dobrać tak, aby była jak najkrótsza lub jak "najtańsza". Mapa, po której porusza się komiwojażer to tak na prawdę graf ważony, którego wierzchołkami są miasta. Jest to również graf pełny, ponieważ wszystkie miasta są ze sobą połączone, a każda droga między nimi ma własny koszt. 

Zadanie komiwojażera jest to sztandarowy problem optymalizacji kombinatorycznej i polega ono na odnalezieniu minimalnego **cyklu Hamiltona** w pełnym grafie ważonym. Cykl Hamiltona to taki cykl w grafie, w którym każdy wierzchołek grafu odwiedzany jest dokładnie raz (oprócz pierwszego wierzchołka). Jednym z możliwych rozwiązań problemu jest wyznaczenie wszystkich możliwych tras i wybór najkrótszej z nich, niestety przy *n-miast* ilość możliwych połączeń wynosi *n!* co sprawia, że nie jest to najbardziej optymalne podejście. Zastosowanie algorytmów nie gwarantuje znalezienia rozwiązania optymalnego, ale odnajdujące, w rozsądnym czasie, rozwiązanie leżące dość blisko optimum. 

## Opis implementacji
Nasza implementacja algorytmu genetycznego do rozwiązania zadania komiwojażera opiera się na czterech klasach: Configs, City, Travel oraz głównej klasie Salesman. Obiekt klasy **City** reprezentuje każde miasto, które posiada swój indeks, pozycję (x,y) oraz listę zawierającą koszt drogi do każdego z pozostałych miast. Lista przy inicjalizacji zapełniana jest wartościami równymi nieskończoność, a następnie wywoływana jest funkcja obliczająca każdą wartość na podstawie wzoru:
```
((first.x - second.x)**2 + (first.y - second.y)**2)**0.5
```
gdzie *first* oraz *second* oznaczają dwa miasta, między którymi obliczany jest koszt trasy. Obiekt klasy **Travel** reprezentuje trasę komiwojażera między wszystkimi miastami. Pierwsza trasa jest generowana losowo, a na jej początku i końcu dodane są wartości 0 oznaczające miejsce, z którego komiwojażer startował i do którego wrócił. Zawiera ona również metodę wyliczającą koszt całej przebytej trasy, która przy inicjalizacji jest równa nieskończoność. Obiekt klasy **Salesman** reprezetuje naszego komiwojażera. Posiada on listę obiektów klasy City oraz listę obiektów klasy Travel, ilość miast oraz tras podana jest w pliku zewnętrznym i klasie **Configs**, jej zawartość podana jest w kodzie poniżej, a opis zmiennych zawarty w komentarzach. Klasa została stworzona dla ułatwienia testowania oraz odpalania algorytmu przez osobę nie znającą kodu projektu.
```
class Configs:
    # dla generowanego datasetu
    map_size = 100
    number_of_cities = 100
    PI = pi

    algorithm_steps = 25000  # Liczba kroków
    number_of_travels = 500  # wielkość populacji

    # ilości krzyżowań
    cruce_num = 20
    pmx_num = 100
    ox_num = 80

    #ilość rotacji
    rotate_num = 20

    # ilość mutacji klasycznych oraz ilość zamienień w każdej
    mutation_num = 100
    mutation_iters = 20
    
    # ilość mutacji - odwracań fragmentów
    revers_mutation_num = 100
```
Klasa Salesman zawiera również metody *plot_cities* oraz *show_best_travel*, które odpowiadają za wizualizację wyników działania algorytmu. Główna metoda, w której realizowane są kolejne kroki algorytmu jest zaprezentowana poniżej. Zawiera ona pętle, w których wykonywane są między innymi mutacje, krzyżowania, rotacje, których ilość wykonań podana jest w klasie Configs. Na końcu wywołania kroku algorytmu sortowane są otrzymane trasy oraz "zabijane" słabsze populacje.
```
def algorithm_step(self):
        for i in range(Configs.cruce_num):
            chosen = random.sample(self.travels, 2)
            self.travels.append(self.crossing(chosen[0], chosen[1]))

        for i in range(Configs.pmx_num):
            chosen = random.sample(self.travels, 2)
            self.travels.append(self.pmx_crossing(chosen[0], chosen[1]))

        for i in range(Configs.mutation_num):
            self.travels.append(self.mutation(random.choice(self.travels)))

        for i in range(Configs.revers_mutation_num):
            self.travels.append(self.revers_mutation(random.choice(self.travels)))

        for i in range(Configs.rotate_num):
            self.travels.append(self.simple_rotate_l(random.choice(self.travels)))

        for i in range(Configs.rotate_num):
            self.travels.append(self.simple_rotate_r(random.choice(self.travels)))

        self.travels.sort(key=lambda t: t.cost)
        if len(self.travels) > Configs.number_of_travels:
                   self.travels = self.travels[:Configs.number_of_travels]
```
Poniżej opis i prezentacja implementacji dwóch najciekawszych metod użytych w naszym algorytmie:
- Mutacja odwrócona - z trasy wycinany jest losowej długości odcinek drogi, gdzie długość oznacza ilość miast, którymi ten odcinek jest połączony, następnie ten odcinek jest (patrząc na listę punktów) lustrzanie odwracny i wstawaiany w to samo miejsce.
```
def revers_mutation(self, travel):
        new_travel = Travel(self.number_of_cities)
        new_travel.vertices_order = travel.vertices_order.copy()
        a, b = self.sorted_pair()
        new_travel.vertices_order[a:b] = new_travel.vertices_order[a:b][::-1]
        new_travel.calculate_cost(self.cities)
        return new_travel
```
- Krzyżowanie PMX (krzyżowanie z częściowym odwozrowaniem) - tworzy się potomka, wybierając podtrasę od jednego rodzica i pozostawiając porządek i pozycje z drugiego rodzica tak długo jak jest to możliwe. Podtrasę wybiera się przez losowe punkty cięcia, które służą jako granice dla operatorów wymieniających. Tworzona jest mapa wymiany, która zawiera pary odpowiadających sobie elementów w obu rodzicach.
```
 def pmx_crossing(self, travel1, travel2):
        new_travel = Travel(self.number_of_cities)
        a, b = self.sorted_pair()

        new_travel.vertices_order = [0] + [None] * (a - 1) + travel1.vertices_order[a:b] + [None] * (self.number_of_cities - b) + [0]

        for i in range(a, b):
            if travel2.vertices_order[i] in new_travel.vertices_order:
                continue
            idx = travel2.vertices_order.index(travel1.vertices_order[i])
            if new_travel.vertices_order[idx] is None:
                new_travel.vertices_order[idx] = travel2.vertices_order[i]
                continue
            idx2 = travel2.vertices_order.index(new_travel.vertices_order[idx])
            if new_travel.vertices_order[idx2] is None:
                new_travel.vertices_order[idx2] = travel2.vertices_order[i]

        for ver in travel2.vertices_order:
            if ver in new_travel.vertices_order:
                continue
            else:
                new_travel.vertices_order[new_travel.vertices_order.index(None)] = ver
        new_travel.calculate_cost(self.cities)
        return new_travel
```
## Wyniki
Zaimplementowany algorytm przetestowaliśmy dwukrotnie na 6 datasetach pochodzących ze strony: [Datasets](http://www.math.uwaterloo.ca/tsp/vlsi/index.html). Podsumowanie uzyskanych wyników znajduje się w tabeli, na zdjęciach zaprezentowane są najlepsze wygenerowane przez nasz algorytm trasy w porównaniu z trasami o minimalnym koszcie oraz wykres pokazujący zmianę długości trasy podczas kroków algorytmu.
|  Liczba punktów |   Koszt trasy I  | Koszt trasy II   | Minimalny koszt  |
|:---:|:----:|:----:|:----:|
| 131 | 612  | 585  | 564  |
| 237 | 1694 | 1136 | 1019 |
| 380 | 1884 | 1823 | 1621 |
| 379 | 1470 | 1423 | 1332 |
| 343 | 1511 | 1433 | 1368 |
| 395 | 1450 | 1454 | 1281 |

### XQF131 - dla 131 punktów
 <div align="center">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/rezultaty2/xqf131-best_road.jpg" width="800">
    <img src="http://www.math.uwaterloo.ca/tsp/vlsi/xqf131.tour.gif" width="800">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/updated_results/xqf131-resultsx.jpg" width="800" alt="Sublime's custom image">
    <br>
</div>

### XQG237 - dla 237 punktów
<div align="center">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/rezultaty2/xqg237-best_road.jpg" width="800">
    <img src="http://www.math.uwaterloo.ca/tsp/vlsi/xqg237.tour.gif" width="800">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/updated_results/xqg237-resultsx.jpg" width="800">
    <br>
</div>

### BCL380 - dla 380 punktów
<div align="center">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/rezultaty2/bcl380-best_road.jpg" width="800">
    <img src="http://www.math.uwaterloo.ca/tsp/vlsi/bcl380.tour.gif">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/updated_results/bcl380-resultsx.jpg" width="800">
    <br>
</div>

### PKA379 - dla 379 punków
<div align="center">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/rezultaty2/pka379-best_road.jpg" width="800">
    <img src="http://www.math.uwaterloo.ca/tsp/vlsi/pka379.tour.gif" width="800">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/updated_results/pka379-resultsx.jpg" width="800">
    <br>
</div>

### PMA343 - dla 343 punktów
<div align="center">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/rezultaty2/pma343-best_road.jpg" width="800">
    <img src="http://www.math.uwaterloo.ca/tsp/vlsi/pma343.tour.gif" width="800">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/updated_results/pma343-resultsx.jpg" width="800">
    <br>
</div>

### PBL395 - dla 395 punktów
<div align="center">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/rezultaty2/pbl395-best_road.jpg" width="800">
    <img src="http://www.math.uwaterloo.ca/tsp/vlsi/pbl395.tour.gif" width="800">
    <img src="https://gitlab.com/mikuczko/algorytmy-genetyczne-projekt/-/raw/master/updated_results/pbl395-resultsx.jpg" width="800">
    <br>
</div>

## Wnioski
- Uzyskane przez nas wyniki są bardzo zbliżone do kosztów najbardziej optymalnych tras dla wszystkich datasetów.
- Działanie algorytmu genetycznego nie jest powtarzalne. Podczas testowania programu dla tego samego zbioru punktów, przy zastosowaniu tych samych parametrów trasy wynikowe były różne.
- Największą poprawę działania algorytmu zaobserowaliśmy po implementacji odwrotnych mutacji oraz krzyżowania PMX. Dzięki nim uzyskaliśmy najlepsze trasy oraz zostały one wyszukane w znacznie krótszym czasie.

## Quickstart
Aby uruchomić algorytm należy wygenerować dataset lub pobrać gotowy ze strony podanej wyżej. (Z pobranego pliku usunąć nagłówki oraz EOF. Przygotowane odpowiednio pliki znajdują się w katalogu **datasets**)

Generowanie losowego datasetu:
```
python main.py --generate
```
Generowanie datasetu na okręgu: 
```
python main.py --generate_circle
```
Wywołanie bez animacji krokowej:
```
python main.py --file_name <ścieżka_datasetu>
```

Wywołanie z animacją krokową:
```
python main.py --file_name <ścieżka_datasetu> --animation
```
### Zmiany parametrów
Wszelkie zmiany parametrów należy dokonywać w skrypcie ``Configs.py``

