from numpy import pi


class Configs:
    # dla generowanego datasetu
    map_size = 100
    number_of_cities = 100
    PI = pi


    algorithm_steps = 25000  # Liczba kroków
    number_of_travels = 500  # wielkość populacji

    # ilości krzyżowań
    cruce_num = 20
    pmx_num = 100
    ox_num = 80

    #ilość rotacji
    rotate_num = 20

    # ilość mutacji klasycznych oraz ilość zamienień w każdej
    mutation_num = 100
    mutation_iters = 20

    # ilość mutacji - odwracań fragmentów
    revers_mutation_num = 100

