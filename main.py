import sys
import random
import argparse
import matplotlib.pyplot as plt

from numpy import cos, sin
from Configs import Configs


def generate_random_vertices():
    with open(f'rnd{Configs.number_of_cities}.tsp', 'w') as file:
        for i in range(Configs.number_of_cities):
            x, y = random.randint(0, Configs.map_size), random.randint(0, Configs.map_size)
            file.write(f'{i + 1}      {x}    {y}\n')


def generate_circle_vertices():
    center = Configs.map_size/2
    divider = Configs.number_of_cities/(2*Configs.PI)
    with open(f'cir{Configs.number_of_cities}.tsp', 'w') as file:
        for i in range(Configs.number_of_cities):
            x, y = int(center + center*cos(i/divider)), int(center + center*sin(i/divider))
            file.write(f'{i + 1}      {x}    {y}\n')


class City:
    def __init__(self, idx, x, y, num):
        self.idx = idx
        self.x = x
        self.y = y
        self.travel_costs = [float('inf')] * num

    def calculate_travel_costs(self, cities):
        for city in cities:
            if self.travel_costs[city.idx] == float('inf'):
                travel_cost = City.calc_single_road(self, city)
                self.travel_costs[city.idx] = travel_cost
                city.travel_costs[self.idx] = travel_cost

    @staticmethod
    def calc_single_road(first, second):
        return ((first.x - second.x)**2 + (first.y - second.y)**2)**0.5


class Travel:
    def __init__(self, num):
        self.vertices_order = list(range(1, num))
        random.shuffle(self.vertices_order)
        self.vertices_order = [0] + self.vertices_order + [0]
        self.cost = float('inf')

    def calculate_cost(self, cities):
        self.cost = 0
        for i in range(len(self.vertices_order) - 1):
            self.cost += cities[self.vertices_order[i]].travel_costs[self.vertices_order[i+1]]


class Salesman:
    def __init__(self, vertices_file, num_of_cities):
        self.number_of_cities = num_of_cities
        self.cities = []
        with open(vertices_file, 'r') as file:
            for i, line in enumerate(file):
                idx_x_y = line.split()
                self.cities.append(City(int(idx_x_y[0]) - 1, int(idx_x_y[1]), int(idx_x_y[2]), self.number_of_cities))
                self.cities[-1].calculate_travel_costs(self.cities)

        self.travels = [Travel(self.number_of_cities) for i in range(Configs.number_of_travels)]
        for travel in self.travels:
            travel.calculate_cost(self.cities)

    def plot_cities(self):
        for city in self.cities:
            plt.scatter(city.x, city.y)
            plt.annotate(str(city.idx), (city.x, city.y))
        plt.draw()

    def show_best_travel(self):
        self.plot_cities()
        x, y = [], []
        for vert in self.travels[0].vertices_order:
            x.append(self.cities[vert].x)
            y.append(self.cities[vert].y)
        plt.plot(x, y)
        plt.pause(0.1)

    def algortihm_step(self):
        for i in range(Configs.cruce_num):
            chosen = random.sample(self.travels, 2)
            self.travels.append(self.crossing(chosen[0], chosen[1]))

        for i in range(Configs.pmx_num):
            chosen = random.sample(self.travels, 2)
            self.travels.append(self.pmx_crossing(chosen[0], chosen[1]))

        for i in range(Configs.mutation_num):
            self.travels.append(self.mutation(random.choice(self.travels)))

        for i in range(Configs.revers_mutation_num):
            self.travels.append(self.revers_mutation(random.choice(self.travels)))

        for i in range(Configs.rotate_num):
            self.travels.append(self.simple_rotate_l(random.choice(self.travels)))

        for i in range(Configs.rotate_num):
            self.travels.append(self.simple_rotate_r(random.choice(self.travels)))

        self.travels.sort(key=lambda t: t.cost)
        if len(self.travels) > Configs.number_of_travels:
            self.travels = self.travels[:Configs.number_of_travels]

    # abcdef -> aCBdef
    def mutation(self, travel):
        new_travel = Travel(self.number_of_cities)
        new_travel.vertices_order = travel.vertices_order.copy()
        for it in range(Configs.mutation_iters):
            a, b = random.randint(1, self.number_of_cities-1), random.randint(1, self.number_of_cities-1)
            new_travel.vertices_order[a], new_travel.vertices_order[b] = new_travel.vertices_order[b], \
                                                                         new_travel.vertices_order[a]
        new_travel.calculate_cost(self.cities)
        return new_travel

    # abcdef -> aEDCBf
    def revers_mutation(self, travel):
        new_travel = Travel(self.number_of_cities)
        new_travel.vertices_order = travel.vertices_order.copy()
        a, b = self.sorted_pair()
        new_travel.vertices_order[a:b] = new_travel.vertices_order[a:b][::-1]
        new_travel.calculate_cost(self.cities)
        return new_travel

    def crossing(self, travel1, travel2):
        new_travel = Travel(self.number_of_cities)
        a, b = self.sorted_pair()
        new_travel.vertices_order = [0] + travel1.vertices_order[a:b].copy()
        for ver in travel2.vertices_order:
            if ver not in new_travel.vertices_order:
                new_travel.vertices_order.append(ver)
        new_travel.vertices_order.append(0)
        new_travel.calculate_cost(self.cities)
        return new_travel

    def ox_crossing(self, travel1, travel2):
        new_travel = Travel(self.number_of_cities)
        a, b = self.sorted_pair()

        new_travel.vertices_order = [0] + [None] * (a - 1) + travel1.vertices_order[a:b] + [None] * (self.number_of_cities - b) + [0]
        for ver in travel2.vertices_order:
            if ver in new_travel.vertices_order:
                continue
            else:
                new_travel.vertices_order[new_travel.vertices_order.index(None)] = ver
        new_travel.calculate_cost(self.cities)
        return new_travel

    def pmx_crossing(self, travel1, travel2):
        new_travel = Travel(self.number_of_cities)
        a, b = self.sorted_pair()

        new_travel.vertices_order = [0] + [None] * (a - 1) + travel1.vertices_order[a:b] + [None] * (self.number_of_cities - b) + [0]

        for i in range(a, b):
            if travel2.vertices_order[i] in new_travel.vertices_order:
                continue
            idx = travel2.vertices_order.index(travel1.vertices_order[i])
            if new_travel.vertices_order[idx] is None:
                new_travel.vertices_order[idx] = travel2.vertices_order[i]
                continue
            idx2 = travel2.vertices_order.index(new_travel.vertices_order[idx])
            if new_travel.vertices_order[idx2] is None:
                new_travel.vertices_order[idx2] = travel2.vertices_order[i]

        for ver in travel2.vertices_order:
            if ver in new_travel.vertices_order:
                continue
            else:
                new_travel.vertices_order[new_travel.vertices_order.index(None)] = ver
        new_travel.calculate_cost(self.cities)
        return new_travel

    # abcdef -> Fabcde
    def simple_rotate_r(self, travel):
        new_travel = Travel(self.number_of_cities)
        new_travel.vertices_order = travel.vertices_order[1:-1].copy()
        new_travel.vertices_order = [0] + new_travel.vertices_order[-1:] + new_travel.vertices_order[:-1] + [0]
        new_travel.calculate_cost(self.cities)
        return new_travel

    # abcdef -> bcdefA
    def simple_rotate_l(self, travel):
        new_travel = Travel(self.number_of_cities)
        new_travel.vertices_order = travel.vertices_order[1:-1].copy()
        new_travel.vertices_order = [0] + new_travel.vertices_order[1:] + new_travel.vertices_order[:1] + [0]
        new_travel.calculate_cost(self.cities)
        return new_travel

    def sorted_pair(self):
        a = random.randint(1, self.number_of_cities - 1)
        b = random.randint(1, self.number_of_cities - 1)
        while a == b:
            b = random.randint(1, self.number_of_cities - 1)
        if a > b:
            return b, a
        else:
            return a, b


def prepare_parser():
    arg_parser = argparse.ArgumentParser(description='Algorytm Genetyczny rozwiązujący problem komiwojazera.')
    arg_parser.add_argument('--file_name', '-f', help='Nazwa pliku do odpalenia.')
    arg_parser.add_argument('--animation', '-a', action='store_true', help='Uruchomienie animacji.')
    arg_parser.add_argument('--generate', '-g', action='store_true', help='Generowanie losowego datasetu.')
    arg_parser.add_argument('--generate_circle', '-gc', action='store_true', help='Generowanie datasetu na planie okregu.')
    return arg_parser


if __name__ == "__main__":

    parser = prepare_parser()
    args = parser.parse_args()

    if args.generate:
        generate_random_vertices()
        sys.exit()
    if args.generate_circle:
        generate_circle_vertices()
        sys.exit()

    number_of_cities = int(args.file_name[3:-4])
    results = []
    komi = Salesman(args.file_name, number_of_cities)
    figure = plt.gcf()
    figure.set_size_inches(12, 8)

    for i in range(Configs.algorithm_steps):
        komi.algortihm_step()
        if i % 100 == 0:
            print(f'Step no.{i}:    {komi.travels[0].cost}')
        results.append(komi.travels[0].cost)
        if args.animation:
            plt.clf()
            komi.show_best_travel()
        if i % 500 == 499:
            plt.clf()
            komi.show_best_travel()
            plt.savefig(f'{args.file_name}-best_road.jpg', dpi=600)
            plt.clf()
            plt.plot(results)
            plt.savefig(f'{args.file_name}-results.jpg')
            with open(f'{args.file_name}-result.txt', 'w') as file:
                file.write(f'{"-".join(list(map(str, komi.travels[0].vertices_order)))}    {komi.travels[0].cost} ')
            print('Saved.')


